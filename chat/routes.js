// This file is required by app.js. It sets up event listeners
// for the two main URL endpoints of the application - /create and /chat/:id
// and listens for socket.io messages.

// Use the gravatar module, to turn email addresses into avatar images:

var gravatar = require('gravatar');

// Export a function, so that we can pass 
// the app and io instances from the app.js file:

module.exports = function(app,io){

	app.get('/', function(req, res){

		// Renderiza views/home.html
		res.render('home');
	});

	app.get('/create', function(req,res){

		// Generar un ID exclusivo para la sala
		var id = Math.round((Math.random() * 1000000));

		// Redirigir a una sala al azar
		res.redirect('/chat/'+id);
	});

	app.get('/chat/:id', function(req,res){

		// Carga la vista chant.html
		res.render('chat');
	});

	// Inicializa una nueva aplicación socket.io, llamado 'chat'
	var chat = io.of('/socket').on('connection', function (socket) {

		// Cuando un cliente emite un evento 'load', responde con el
		// número de personas en esta sala de chat

		socket.on('load',function(data){

			var room = findClientsSocket(io,data,'/socket');
			if(room.length === 0 ) {

				socket.emit('peopleinchat', {number: 0});
			}
			else if(room.length === 1) {

				socket.emit('peopleinchat', {
					number: 1,
					user: room[0].username,
					avatar: room[0].avatar,
					id: data
				});
			}
			else if(room.length === 2) {

				socket.emit('peopleinchat', {
					number: 2,
					user: room[1].username,
					avatar: room[1].avatar,
					id: data
				});
			}
			else if(room.length === 3) {

				socket.emit('peopleinchat', {
					number: 3,
					user: room[2].username,
					avatar: room[2].avatar,
					id: data
				});
			}
			else if(room.length === 4) {

				socket.emit('peopleinchat', {
					number: 4,
					user: room[3].username,
					avatar: room[3].avatar,
					id: data
				});
			}
			else if(room.length === 5) {

				socket.emit('peopleinchat', {
					number: 5,
					user: room[4].username,
					avatar: room[4].avatar,
					id: data
				});
			}
			else if(room.length == 6) {

				chat.emit('tooMany', {boolean: true});
			}
		});

		// When the client emits 'login', save his name and avatar,
		// and add them to the room
		socket.on('login', function(data) {

			var room = findClientsSocket(io, data.id, '/socket');
			// Only two people per room are allowed
			if (room.length < 5) {

				// Use the socket object to store data. Each client gets
				// their own unique socket object

				socket.username = data.user;
				socket.room = data.id;
				socket.avatar = gravatar.url(data.avatar, {s: '140', r: 'x', d: 'mm'});

				// Tell the person what he should use for an avatar
				socket.emit('img', socket.avatar);


				// Añade un nuevo cliente a la sala
				socket.join(data.id);

				if (room.length >= 1) {

					var usernames = [],
						avatars = [];

					usernames.push(room[0].username);
					usernames.push(socket.username);

					avatars.push(room[0].avatar);
					avatars.push(socket.avatar);

					// Enviar un evento startChat a todas las personas de la
					// sala, junto con la lista de personas que esta en él.

					chat.in(data.id).emit('startChat', {
						boolean: true,
						id: data.id,
						users: usernames,
						avatars: avatars
					});
				}
			}
			else {
				socket.emit('tooMany', {boolean: true});
			}
		});

		// Cuando alguien deja el chat
		socket.on('disconnect', function() {

			// Notifica a la otra persona den la sala del chat
			// que el otro a abandonado la sala

			socket.broadcast.to(this.room).emit('leave', {
				boolean: true,
				room: this.room,
				user: this.username,
				avatar: this.avatar
			});

			// Sal de la sala
			socket.leave(socket.room);
		});


		// Manejo de envío de mensajes
		socket.on('msg', function(data){

			// When the server receives a message, it sends it to the other person in the room.
			socket.broadcast.to(socket.room).emit('receive', {msg: data.msg, user: data.user, img: data.img});
		});
	});
};

function findClientsSocket(io,roomId, namespace) {
	var res = [],
		ns = io.of(namespace ||"/");    // the default namespace is "/"

	if (ns) {
		for (var id in ns.connected) {
			if(roomId) {
				var index = ns.connected[id].rooms.indexOf(roomId) ;
				if(index !== -1) {
					res.push(ns.connected[id]);
				}
			}
			else {
				res.push(ns.connected[id]);
			}
		}
	}
	return res;
}