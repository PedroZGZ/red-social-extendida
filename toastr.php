<link href="toastr/toastr.css" rel="stylesheet" type="text/css" />
<script src="toastr/jquery-1.9.1.min.js"></script>
<script src="toastr/toastr.js"></script>
<?php
echo '
<script>
$(document).ready(function() {
	var msg = "'.$subtitulo.'";
	var title = "'.$titulo.'";
	var options = {
		"closeButton": true,
		"debug": false,
		"positionClass": "toast-top-right",
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "25000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	};
	toastr.'.$tipo.'(msg, title, options);
});
</script>';
/*
$titulo = "buenas";
$subtitulo = "buenas";
$tipo = "error";
require "toastr.php";

Rojo.: error
Verde.: success
Azul.: info
Amarillo.: warning
*/
?>