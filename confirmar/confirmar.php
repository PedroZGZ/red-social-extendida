<?php
session_start();
try {
	$db = new PDO("sqlite:../base de datos.s3db");
} catch (PDOException $e) {
	echo "Error: No se puede conectar. " . $e->getMessage();
}
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if((!isset($_SESSION['usr'])) && (!isset($_COOKIE['usr']))){
	if(isset($_POST["confirmar"])){

			$sql = "SELECT usuario FROM usuarios WHERE confirmado='".$_POST['usuario_confirmar']."'";
			$resultados = $db->query($sql);
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$usuario = "{$usuario}";
			}
			
			try{
				$editar = "UPDATE usuarios SET confirmado=:confirmado WHERE confirmado=:buscar";
				$result = $db->prepare($editar);
				$result->execute(array(":confirmado" => "confirmado", ":buscar" => $_POST['usuario_confirmar']));

			}catch(Exception $e){
				$registrarse = "no";
				$titulo = "Error";
				$subtitulo = "Tu usuario a caducado";
				$tipo = "error";
				require "toastr.php";
			}
			
			if(!isset($registrarse)){
				$_SESSION['usr'] = $usuario;
				header("Location: ../index.php");
			}
			
		}
}
?>