<?php
session_start();
include "encriptacion/encriptacion.php";
require "recaptcha/recaptchalib.php";
require_once "vendor/twig/twig/lib/Twig/Autoloader.php";
Twig_Autoloader::register();

if((isset($_POST["chat"])) || (isset($_POST["ir_sala"]))){
	if(isset($_POST["ir_sala"])){
		if(!isset($_SESSION['chat'])){
			$_SESSION['chat'] = $_POST["sala"];
			header("Location: http://localhost/");
			exit;
		}else{
			$_SESSION['chat'] = $_POST["sala"];
		}
	}else{
		if(!isset($_SESSION['chat'])){
			$_SESSION['chat'] = "chat";
			header("Location: http://localhost/");
			exit;
		}else{
			$_SESSION['chat'] = "chat";
		}
	}
	if(!isset($_SESSION['chat'])){
		header("Location: https://localhost/");
		exit;
	}
}

if((!isset($_SERVER['HTTPS'])) && (!isset($_SESSION['chat']))){
	header("Location: https://localhost/");
	exit;
}


try {
	$db = new PDO("sqlite:base de datos.s3db");
} catch (PDOException $e) {
	echo "Error: No se puede conectar. " . $e->getMessage();
}

$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

if(isset($_COOKIE['usr'])){
	$_SESSION['usr'] = $_COOKIE['usr'];
	//setCookie('usr', '', time() - 1000);
}
if(isset($_SESSION['usr'])){
	setcookie('usr', $_SESSION['usr'], time() + 86400);
}

try{
	if(!isset($_SESSION['usr'])){
		// ---------------------------------------------------------- Alta ----------------------------------------------------------
		if(isset($_POST["confirmar"])){
			
			
			
			$sql = "SELECT usuario FROM usuarios WHERE confirmado='".$_POST['usuario_confirmar']."'";
			$resultados = $db->query($sql);
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$usuario = "{$usuario}";
			}
			
			try{
				$editar = "UPDATE usuarios SET confirmado=:confirmado WHERE confirmado=:buscar";
				$result = $db->prepare($editar);
				$result->execute(array(":confirmado" => "confirmado", ":buscar" => $_POST['usuario_confirmar']));

			}catch(Exception $e){
				$registrarse = "no";
				$titulo = "Error";
				$subtitulo = "Tu usuario a caducado";
				$tipo = "error";
				require "toastr.php";
			}
			
			if(!isset($registrarse)){
				$_SESSION['usr'] = $usuario;
			}
			
		}
		
		if(isset($_POST["signup"])){
			$privatekey = "6Ld_s_0SAAAAAAFgDiT59L8uhMjnwHUaNgH3mlku";
			
			$resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"],
			$_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
			
			if (empty($_POST["recaptcha_response_field"])){ /* Si el captcha está vacío */
				$titulo = "Error";
				$subtitulo = "El captcha esta vacío";
				$tipo = "error";
				require "toastr.php";
			}else if(!$resp->is_valid){ /* Si el captcha es incorrecto */
				$titulo = "Error";
				$subtitulo = "El captcha esta mal puesto";
				$tipo = "error";
				require "toastr.php";
			}else{
				$nombres = $_POST['nombres'];
				$apellidos = $_POST['apellidos'];
				$nombre = $nombres . " " . $apellidos;
				
				$usuario = $_POST['usuario'];
				$usuario_minusculas = strtolower($usuario);
				$email = $_POST['email'];
				$telefono = $_POST['telefono'];
				$password = $_POST['password'];
				$nacionalidad = $_POST['nacionalidad'];
				$fecha = $_POST['fecha'];
				$hoy = date("Y-m-d");
				$descripcion = $_POST['descripcion'];
				$confirmar = md5($usuario);
				
				
				try{
					$insertar = "INSERT INTO usuarios (usuario, usuario_minusculas, clave, nombre, email, telefono, fecha_registrado,
								fecha_nacimiento, genero, nacionalidad, descripcion, confirmado)
						VALUES(:usuario, :usuario_minusculas, :clave, :nombre, :email, :telefono, :fecha_registrado, :fecha_nacimiento,
								:genero, :nacionalidad, :descripcion, :confirmado)";
					$sentencia = $db->prepare($insertar);
					
					
					$sentencia->execute(array(
					":usuario" => $usuario, ":usuario_minusculas" => $usuario_minusculas, ":clave" => Encrypter::encrypt(cryptPass($password)),
					":nombre" => $nombre, ":email" => $email, ":telefono" => $telefono, ":fecha_registrado" => $hoy, ":fecha_nacimiento" => $fecha,
					":genero" => $_POST['genero'], ":nacionalidad" => $nacionalidad, ":descripcion" => $descripcion, ":confirmado" => md5($usuario)));
					
					mkdir("galerias/fotos_".$usuario);
					mkdir("galerias/fotos_".$usuario."/avatar");
				}catch(Exception $e){
					$registrarse = "no";
					$titulo = "Error";
					$subtitulo = "Algun dato esta repetido";
					$tipo = "error";
					require "toastr.php";
				}
				
				if(!isset($registrarse)){
					//$_SESSION['usr'] = $usuario;
					
					$codigohtml = '
					<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
					<br /><br />
							<div style="width: 12.66666667%;
							float: left; position: relative; min-height: 1px; padding-right: 15px; padding-left: 15px;"></div>
							<div style="width: 80%;background-color: rgb(238,238,238); border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px;
							float: left; position: relative; min-height: 1px;padding-right: 15px; padding-left: 15px;">
								<h2>Confirmar incripcion por <span style="color: #777;">e-mail.</span></h2>
								<p style="margin-bottom: 20px;font-size: 16px;font-weight: 300;line-height: 1.4;font-size: 21px;">
								Para activar la cuenta de usuario <strong>'.$usuario.'</strong> por favor haga click en el boton de abajo, muchas gracias.
								</p>
								
								<form action="http://localhost/confirmar/confirmar.php" method="post">
									<input name="usuario_confirmar" type="hidden" value="'.md5($usuario).'" /><br />
									<input type="submit" name="confirmar" value="Confirmar" style=" background: #83ff70;
									  background-image: -webkit-linear-gradient(top, #83ff70, #2bb82f);
									  background-image: -moz-linear-gradient(top, #83ff70, #2bb82f);
									  background-image: -ms-linear-gradient(top, #83ff70, #2bb82f);
									  background-image: -o-linear-gradient(top, #83ff70, #2bb82f);
									  background-image: linear-gradient(to bottom, #83ff70, #2bb82f);
									  -webkit-border-radius: 8; -moz-border-radius: 8; border-radius: 8px; font-family: Arial;
									  color: #ffffff; font-size: 20px; padding: 10px 20px 10px 20px; text-decoration: none;" />
								</form>
								<br /><br />
							</div>
						  </div>
					';
					//pedrovera.infenlaces.com/index.php
					//$email = "pedroaureliozgz@gmail.com";
					$asunto = 'Confirmar cuenta.';
					$cabeceras = "Content-type: text/html\r\n";

					mail($email,$asunto,$codigohtml,$cabeceras);
					
					
					$texto_informar = "acceder";
				}

				
				$nombre = $_FILES['imagen']['name'];
				$tmp_name = $_FILES['imagen']['tmp_name'];
				
				$error = $_FILES['imagen']['error'];
				$size = $_FILES['imagen']['size'];
				$max_size = 1024 * 1024 * 1;
				$type = $_FILES['imagen']['type'];

				if(!$error){
					if ($error){
						$titulo = "Error";
						$subtitulo = "Se ha producido un error";
						$tipo = "error";
						require "toastr.php";
					}else if ($size > $max_size){
						$titulo = "Error";
						$subtitulo = "Error, el tamaño máximo permitido es un 1MB";
						$tipo = "error";
						require "toastr.php";
					}else if($type != "image/jpeg" && $type != "image/jpg" && $type != "image/png" && $type != "image/gif"){
						$titulo = "Error";
						$subtitulo = "El archivo no es una imagen";
						$tipo = "error";
						require "toastr.php";
					}else{
						$ruta = "galerias/fotos_".$usuario."/avatar/".$nombre;
						move_uploaded_file($tmp_name, $ruta);
						
						$id = $db->lastInsertId();
						$editar = "UPDATE usuarios SET avatar=:avatar WHERE id_usuario=:id";
						$result = $db->prepare($editar);
						$result->execute(array(":avatar" => $nombre, ":id" => $id));
					}
				}else{
					$titulo = "Error";
					$subtitulo = "Se ha producido un error";
					$tipo = "error";
				}
			}
		}
		// ---------------------------------------------------------- Alta ----------------------------------------------------------
		
		// ---------------------------------------------------------- Registrarse ----------------------------------------------------------
		if(isset($_POST["entrar"])){
			
			
			$sql = "SELECT * FROM usuarios WHERE usuario='".$_POST['usuario']."'";
			$resultados = $db->query($sql);
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$confirmado = "{$confirmado}";
			
				if(($confirmado) == "confirmado"){
			
						
						if(crypt($_POST['pass'], Encrypter::decrypt("{$clave}")) == Encrypter::decrypt("{$clave}")){
							$_SESSION['usr'] = $_POST['usuario'];
							$titulo = "Bienvenido";
							$subtitulo = "Bienvenido ".$_SESSION['usr'];
							$tipo = "success";
							require "toastr.php";
						}else{
							$titulo = "Error";
							$subtitulo = "Contraseña incorrecta";
							$tipo = "error";
							require "toastr.php";
						}
					
					
				}else{
					$titulo = "Error";
					$subtitulo = "No has confirmado por e-mail";
					$tipo = "error";
					require "toastr.php";
				}
			}
		}
		// ---------------------------------------------------------- Fin registrarse ----------------------------------------------------------

		// ---------------------------------------------------------- Si te has registrado ----------------------------------------------------------

	}
	if(isset($_SESSION['usr'])){
		// ---------------------------------------------------------- Amigos ----------------------------------------------------------
		if(isset($_POST["agregar_amigo"])){ //Agregar amigos
			
			if($_POST["insertar_amigo"] != ""){
				$sql = "SELECT id_usuario FROM usuarios WHERE usuario='".$_SESSION['usr']."'";
				$resultados = $db->query($sql);
				while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
					extract($row);
					$id_invitador = "{$id_usuario}";
				}
				
				$sql = "SELECT id_usuario FROM usuarios WHERE usuario='".$_POST["insertar_amigo"]."'";
				$resultados = $db->query($sql);
				while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
					extract($row);
					$id_amigo = "{$id_usuario}";
				}
				if(!isset($id_amigo)){
					$titulo = "Usuario desconocido";
					$subtitulo = "No existe tal usuario";
					$tipo = "warning";
					require "toastr.php";
				}else{
					$sql = "SELECT * FROM amigos WHERE id_usuario_elegidor=".$id_invitador." AND id_usuario_amigo=".$id_amigo;
					$resultados = $db->query($sql);
					while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
						extract($row);
						$fallo = "{$id_amigo}";
					}
					if($id_invitador != $id_amigo){
						if(!isset($fallo)){
							$insertar = "INSERT INTO amigos (id_usuario_elegidor, id_usuario_amigo)
								VALUES(:id_usuario_elegidor, :id_usuario_amigo)";
							$sentencia = $db->prepare($insertar);
							
							$sentencia->execute(array(
							":id_usuario_elegidor" => $id_invitador, ":id_usuario_amigo" => $id_amigo));
							
							$titulo = "Nuevo amigo";
							$subtitulo = "Nuevo amigo registrado";
							$tipo = "success";
							require "toastr.php";
						}else{
							$titulo = "Error";
							$subtitulo = "Amigo repetido";
							$tipo = "error";
							require "toastr.php";
						}
					}else{
						$titulo = "Error";
						$subtitulo = "No puedes meter tu propio usuario";
						$tipo = "error";
						require "toastr.php";
					}
				}
			}else{
				$titulo = "Fallo";
				$subtitulo = "No has metido el usuario";
				$tipo = "info";
				require "toastr.php";
			}
			
		}elseif(isset($_POST["borrar_amigo"])){ //Borrar amigos
			$borrar = "DELETE FROM amigos WHERE id_amigo=:id_amigo";
			$result = $db->prepare($borrar);
			$result->execute(array(":id_amigo" => $_POST["borrar_amigo"]));
			
			$titulo = "Borrado";
			$subtitulo = "Amigo borrado";
			$tipo = "info";
			require "toastr.php";
			
		// ---------------------------------------------------------- Fin amigos ----------------------------------------------------------
		// ---------------------------------------------------------- Principal ----------------------------------------------------------
	
		}
		if(isset($_POST["nuevo_tema"])){
			
			$sql = "SELECT * FROM temas";
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$temas[] = $row;
			}
			
			try{
				$insertar = "INSERT INTO temas (titulo, descripcion)
					VALUES(:titulo, :descripcion)";
				$sentencia = $db->prepare($insertar);
				
				
				$sentencia->execute(array(
				":titulo" => $_POST["titulo"], ":descripcion" => $_POST["descripcion"]));
				
				
			}catch(Exception $e){
				$registrarse = "no";
				$titulo = "Error";
				$subtitulo = "Algun dato esta repetido";
				$tipo = "error";
				require "toastr.php";
				$nuevo_tema = "no";
			}
			
			$nombre = $_FILES['imagen']['name'];
			$tmp_name = $_FILES['imagen']['tmp_name'];
			
			$error = $_FILES['imagen']['error'];
			$size = $_FILES['imagen']['size'];
			$max_size = 1024 * 1024 * 1;
			$type = $_FILES['imagen']['type'];
			
			
			if(!$error){
				if ($error){
					$titulo = "Error";
					$subtitulo = "Se ha producido un error";
					$tipo = "error";
					require "toastr.php";
				}else if ($size > $max_size){
					$titulo = "Error";
					$subtitulo = "Error, el tamaño máximo permitido es un 1MB";
					$tipo = "error";
					require "toastr.php";
				}else if($type != "image/jpeg" && $type != "image/jpg" && $type != "image/png" && $type != "image/gif"){
					$titulo = "Error";
					$subtitulo = "El archivo no es una imagen";
					$tipo = "error";
					require "toastr.php";
				}else{
					$ruta = "imagenes/".$nombre;
					move_uploaded_file($tmp_name, $ruta);
					
					$id = $db->lastInsertId();
					$editar = "UPDATE temas SET imagen=:imagen WHERE id_tema=:id";
					$result = $db->prepare($editar);
					$result->execute(array(":imagen" => $nombre, ":id" => $id));
				}
			}else{
				$titulo = "Error";
				$subtitulo = "Se ha producido un error";
				$tipo = "error";
			}
			
			$sql = "SELECT * FROM usuarios WHERE usuario='".$_SESSION['usr']."'";
			$consulta   = $db->query($sql);
			$resultados = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$datos_usuario[] = $row;
			}
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$id_usuario = "{$id_usuario}";
			}
			
			$sql = "SELECT * FROM amigos_usuario WHERE id_usuario=".$id_usuario;
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$amigos_usuario[] = $row;
			}
			
			$sql = "SELECT * FROM temas";
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$temas[] = $row;
			}
			
//-----------------------------------------------------
			$contador = 0;
			$id_tema = $db->lastInsertId();
			$sql = "SELECT * FROM foro WHERE id_tema=".$id_tema;
			$consulta = $db->query($sql);
			$comentarios = array();
			while ($row = $consulta->fetch()) {
				extract($row);
				$contador = $contador + 1;
				
				$cadena = "{$fecha}"; //DB
				$resultado = str_replace("/", "-", $cadena);
				$fecha_escrito = new DateTime($resultado);
				$hoy = new DateTime("now"); //Actual
				$diferencia = $hoy->diff($fecha_escrito);

				$diferencia_anos = (int)$diferencia->format('%y'); //años
				$diferencia_meses = (int)$diferencia->format('%m'); //meses
				$diferencia_horas = ((int)$diferencia->format('%h')-1); //horas
				if($diferencia_horas == -1){
					$diferencia_dias = ((int)$diferencia->format('%d')-1); //dias
					$diferencia_horas = 23; //horas
				}else{
					$diferencia_dias = (int)$diferencia->format('%d'); //dias
				}
				$diferencia_minutos = (int)$diferencia->format('%i'); //minutos
				$diferencia_segundos = (int)$diferencia->format('%s'); //segundos

				if($diferencia_anos == 0){
					if($diferencia_meses == 0){
						if($diferencia_dias == 0){
							if($diferencia_horas == 0){
								if($diferencia_minutos == 0){
									if($diferencia_segundos == 0){
										$mensaje_diferencia= "Recientemente";
									}else{
										$mensaje_diferencia= $diferencia_segundos." segundos";
									}
								}else{
									$mensaje_diferencia= $diferencia_minutos." minutos y ".$diferencia_segundos." segundos";
								}
							}else{
								$mensaje_diferencia= $diferencia_horas." horas y ".$diferencia_minutos." minutos";
							}
						}else{
							$mensaje_diferencia= $diferencia_dias." dias y ".$diferencia_horas." horas";
						}
					}else{
						$mensaje_diferencia= $diferencia_meses." meses y ".$diferencia_dias." dias";
					}
				}else{
					$mensaje_diferencia= "Mas de ".$diferencia_anos." años";
				}
				$comentarios["$contador"] = $row;
				$comentarios["$contador"]["tiempo"] = $mensaje_diferencia;
			}

//------------------------------
			
		
		// ---------------------------------------------------------- Foro ----------------------------------------------------------
		}elseif((isset($_POST["ver_tema"])) || (isset($_POST["comentar"])) || (isset($_POST["recargar_foro"]))){
		
			
			$sql = "SELECT * FROM usuarios WHERE usuario='".$_SESSION['usr']."'";
			$consulta   = $db->query($sql);
			$resultados = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$datos_usuario[] = $row;
			}
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$id_usuario = "{$id_usuario}";
			}
			
			$sql = "SELECT * FROM temas";
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$temas[] = $row;
			}
			
			$sql = "SELECT * FROM amigos_usuario WHERE id_usuario=".$id_usuario;
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$amigos_usuario[] = $row;
			}
			
			if(isset($_POST["comentar"])){
				$sql = "SELECT id_usuario FROM usuarios WHERE usuario='".$_SESSION['usr']."'";
				$resultados = $db->query($sql);
				while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
					extract($row);
					$id_usuario = "{$id_usuario}";
				}
			
			
				try{
					$insertar = "INSERT INTO comentarios (id_tema, redactor, texto)
						VALUES(:id_tema, :redactor, :texto)";
					$sentencia = $db->prepare($insertar);
					
					$sentencia->execute(array(
					":id_tema" => $_POST["id_tema"], ":redactor" => $id_usuario, ":texto" => $_POST["ckeditor"]));
				}catch(Exception $e){
					$titulo = "Error";
					$subtitulo = "Tu comentario ess invalido";
					$tipo = "error";
					require "toastr.php";
				}
			}
			

			$id_tema = $_POST["id_tema"];
	
			$contador = 0;
			$sql = "SELECT * FROM foro WHERE id_tema=".$id_tema;
			$consulta = $db->query($sql);
			$comentarios = array();
			while ($row = $consulta->fetch()) {
				extract($row);
				$contador = $contador + 1;

					
				//
				$cadena = "{$fecha}"; //DB
				$resultado = str_replace("/", "-", $cadena);
				$fecha_escrito = new DateTime($resultado);
				$hoy = new DateTime("now"); //Actual
				$diferencia = $hoy->diff($fecha_escrito);

				$diferencia_anos = (int)$diferencia->format('%y'); //años
				$diferencia_meses = (int)$diferencia->format('%m'); //meses
				$diferencia_horas = ((int)$diferencia->format('%h')-1); //horas
				if($diferencia_horas == -1){
					$diferencia_dias = ((int)$diferencia->format('%d')-1); //dias
					$diferencia_horas = 23; //horas
				}else{
					$diferencia_dias = (int)$diferencia->format('%d'); //dias
				}
				$diferencia_minutos = (int)$diferencia->format('%i'); //minutos
				$diferencia_segundos = (int)$diferencia->format('%s'); //segundos

				if($diferencia_anos == 0){
					if($diferencia_meses == 0){
						if($diferencia_dias == 0){
							if($diferencia_horas == 0){
								if($diferencia_minutos == 0){
									if($diferencia_segundos == 0){
										$mensaje_diferencia= "Recientemente";
									}else{
										$mensaje_diferencia= $diferencia_segundos." segundos";
									}
								}else{
									$mensaje_diferencia= $diferencia_minutos." minutos y ".$diferencia_segundos." segundos";
								}
							}else{
								$mensaje_diferencia= $diferencia_horas." horas y ".$diferencia_minutos." minutos";
							}
						}else{
							$mensaje_diferencia= $diferencia_dias." dias y ".$diferencia_horas." horas";
						}
					}else{
						$mensaje_diferencia= $diferencia_meses." meses y ".$diferencia_dias." dias";
					}
				}else{
					$mensaje_diferencia= "Mas de ".$diferencia_anos." años";
				}
				$comentarios["$contador"] = $row;
				$comentarios["$contador"]["tiempo"] = $mensaje_diferencia;
			}
		
		// ---------------------------------------------------------- Cerrar ----------------------------------------------------------
		
		}elseif(isset($_POST["cerrar"])){
			session_destroy();
			setCookie('usr', '', time() - 1000);
		
		
		// ---------------------------------------------------------- Fin cerrar ----------------------------------------------------------
		// ---------------------------------------------------------- Galería ----------------------------------------------------------
		
		}elseif((isset($_POST["galeria"])) || (isset($_POST["recargar_galeria"]))){
			$sql = "SELECT * FROM usuarios WHERE usuario='".$_SESSION['usr']."'";
			$consulta   = $db->query($sql);
			$resultados = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$datos_usuario[] = $row;
			}
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$id_usuario = "{$id_usuario}";
				$mi_id_usuario = "{$id_usuario}";
			}
			$sql = "SELECT * FROM amigos_usuario WHERE id_usuario=".$_POST["id_usuario"]." AND id_amigo=".$mi_id_usuario;
			$resultados = $db->query($sql);
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$amigo = "{$id_relacion}";
			}
			$sql = "SELECT * FROM temas";
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$temas[] = $row;
			}
			
			$sql = "SELECT * FROM amigos_usuario WHERE id_usuario=".$mi_id_usuario;
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$amigos_usuario[] = $row;
			}
			
			$id_usuario = $_POST["id_usuario"];
			$sql = "SELECT * FROM usuarios WHERE id_usuario=".$_POST["id_usuario"];
			$resultados = $db->query($sql);
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$tipo_galeria = "{$tipo_galeria}";
				$usuario = "{$usuario}";
			}
			
			$contador = 0;
			$sql = "SELECT * FROM galerias WHERE id_usuario=".$id_usuario;
			$consulta = $db->query($sql);
			$galeria = array();
			while ($row = $consulta->fetch()) {
				extract($row);
				$contador = $contador + 1;
				
				$comprobar = $contador % 10;
				
				
				$galeria["$contador"] = $row;
				$galeria["$contador"]["carpeta"] = $usuario;
				if(($comprobar==0) || ($contador==1)){
					$galeria["$contador"]["tamano"] = "grande";
				}
				if($contador == 1){
					$portada = $imagen;
				}
			}
			
		}elseif(isset($_POST["subirImagenGaleria"])){
			$sql = "SELECT * FROM usuarios WHERE usuario='".$_SESSION['usr']."'";
			$consulta   = $db->query($sql);
			$resultados = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$datos_usuario[] = $row;
			}
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$id_usuario = "{$id_usuario}";
			}
			
			$sql = "SELECT * FROM temas";
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$temas[] = $row;
			}
			
			$sql = "SELECT * FROM amigos_usuario WHERE id_usuario=".$id_usuario;
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$amigos_usuario[] = $row;
			}
			
			$id_usuario = $_POST["id_usuario"];
			$sql = "SELECT usuario FROM usuarios WHERE id_usuario=".$_POST["id_usuario"];
			$resultados = $db->query($sql);
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$usuario = "{$usuario}";
			}
			
			$nombre = $_FILES['imagen']['name'];
			$tmp_name = $_FILES['imagen']['tmp_name'];
			
			$error = $_FILES['imagen']['error'];
			$size = $_FILES['imagen']['size'];
			$max_size = 1024 * 1024 * 1;
			$type = $_FILES['imagen']['type'];
			if(!$error){
				if ($error){
					$titulo = "Error";
					$subtitulo = "Se ha producido un error";
					$tipo = "error";
					require "toastr.php";
				}else if ($size > $max_size){
					$titulo = "Error";
					$subtitulo = "Error, el tamaño máximo permitido es un 1MB";
					$tipo = "error";
					require "toastr.php";
				}else if($type != "image/jpeg" && $type != "image/jpg" && $type != "image/png" && $type != "image/gif"){
					$titulo = "Error";
					$subtitulo = "El archivo no es una imagen";
					$tipo = "error";
					require "toastr.php";
				}else{
					$ruta = "galerias/fotos_".$usuario."/".$nombre;
					move_uploaded_file($tmp_name, $ruta);
					
					$insertar = "INSERT INTO galerias (id_usuario, imagen)
					VALUES(:id_usuario, :imagen)";
					$sentencia = $db->prepare($insertar);
					$sentencia->execute(array(
					":id_usuario" => $id_usuario, ":imagen" => $nombre));
				}
			}else{
				$titulo = "Error";
				$subtitulo = "Se ha producido un error";
				$tipo = "error";
			}
			
			$contador = 0;
			$sql = "SELECT * FROM galerias WHERE id_usuario=".$id_usuario;
			$consulta = $db->query($sql);
			$galeria = array();
			while ($row = $consulta->fetch()) {
				extract($row);
				$contador = $contador + 1;
				$comprobar = $contador % 10;
				
				$galeria["$contador"] = $row;
				$galeria["$contador"]["carpeta"] = $usuario;
				if(($comprobar==0) || ($contador==1)){
					$galeria["$contador"]["tamano"] = "grande";
				}
				if($contador == 1){
					$portada = "{$imagen}";
				}
			}
		}elseif((isset($_POST["modoBorrar"])) || (isset($_POST["borrar_imagen"]))){ //----------------------
			$usuario = $_SESSION['usr'];
			
			$sql = "SELECT * FROM usuarios WHERE usuario='".$_SESSION['usr']."'";
			$consulta   = $db->query($sql);
			$resultados = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$datos_usuario[] = $row;
			}
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$id_usuario = "{$id_usuario}";
			}
			
			$sql = "SELECT * FROM temas";
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$temas[] = $row;
			}
			
			$sql = "SELECT * FROM amigos_usuario WHERE id_usuario=".$id_usuario;
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$amigos_usuario[] = $row;
			}
			
			$sql = "SELECT id_usuario FROM usuarios WHERE usuario='".$_SESSION['usr']."'";
			$resultados = $db->query($sql);
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$id_usuario = "{$id_usuario}";
			}
			
			if(isset($_POST["borrar_imagen"])){
				$sql = "SELECT * FROM galerias WHERE id_galeria=".$_POST["borrar_imagen"];
				$resultados = $db->query($sql);
				while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
					extract($row);
					$imagen = "{$imagen}";
				}
				
				$borrar = "DELETE FROM galerias WHERE id_galeria=:id_galeria";
				$result = $db->prepare($borrar);
				$result->execute(array(":id_galeria" => $_POST["borrar_imagen"]));
				
				unlink("galerias/fotos_".$_SESSION['usr']."/".$imagen);
				
				
			}
			
			$contador = 0;
			$sql = "SELECT * FROM galerias WHERE id_usuario=".$id_usuario;
			$consulta = $db->query($sql);
			$galeria = array();
			while ($row = $consulta->fetch()) {
				extract($row);
				$contador = $contador + 1;
				$comprobar = $contador % 10;
				
				$galeria["$contador"] = $row;
				$galeria["$contador"]["carpeta"] = $_SESSION['usr'];
				if(($comprobar==0) || ($contador==1)){
					$galeria["$contador"]["tamano"] = "grande";
				}
				if($contador == 1){
					$portada = "{$imagen}";
				}
			}
		}elseif(isset($_POST["tipo_galeria"])){
			$usuario = $_SESSION['usr'];
			
			$editar = "UPDATE usuarios SET tipo_galeria=:tipo_galeria WHERE usuario=:usuario";
			$result = $db->prepare($editar);
			$result->execute(array(":tipo_galeria" => $_POST["tipo_galeria"], ":usuario" => $_SESSION['usr']));
			
			$sql = "SELECT * FROM usuarios WHERE usuario='".$_SESSION['usr']."'";
			$consulta   = $db->query($sql);
			$resultados = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$datos_usuario[] = $row;
			}
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$id_usuario = "{$id_usuario}";
			}
			
			$sql = "SELECT * FROM temas";
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$temas[] = $row;
			}
			
			$sql = "SELECT * FROM amigos_usuario WHERE id_usuario=".$id_usuario;
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$amigos_usuario[] = $row;
			}
			
			$sql = "SELECT id_usuario FROM usuarios WHERE usuario='".$_SESSION['usr']."'";
			$resultados = $db->query($sql);
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$id_usuario = "{$id_usuario}";
			}
			
			$id_usuario = $_POST["id_usuario"];
			$sql = "SELECT usuario FROM usuarios WHERE id_usuario=".$_POST["id_usuario"];
			$resultados = $db->query($sql);
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$usuario = "{$usuario}";
			}
			
			$contador = 0;
			$sql = "SELECT * FROM galerias WHERE id_usuario=".$id_usuario;
			$consulta = $db->query($sql);
			$galeria = array();
			while ($row = $consulta->fetch()) {
				extract($row);
				$contador = $contador + 1;
				
				$comprobar = $contador % 10;
				

				
				$galeria["$contador"] = $row;
				$galeria["$contador"]["carpeta"] = $_SESSION['usr'];
				if(($comprobar==0) || ($contador==1)){
					$galeria["$contador"]["tamano"] = "grande";
				}
				if($contador == 1){
					$portada = "{$imagen}";
				}
			}
			
		// ---------------------------------------------------------- Fin Galería ----------------------------------------------------------
		// ---------------------------------------------------------- Mostrar datos perfiles ----------------------------------------------------------
		}elseif((isset($_POST["datos_personales"])) || (isset($_POST["recargar_formulario"]))){
			$sql = "SELECT * FROM usuarios WHERE usuario='".$_SESSION['usr']."'";
			$consulta   = $db->query($sql);
			$resultados = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$datos_usuario[] = $row;
			}
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$id_usuario = "{$id_usuario}";
			}
			
			$sql = "SELECT * FROM temas";
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$temas[] = $row;
			}
			
			$sql = "SELECT * FROM amigos_usuario WHERE id_usuario=".$id_usuario;
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$amigos_usuario[] = $row;
			}
		// ---------------------------------------------------------- Fin datos perfiles ----------------------------------------------------------
		// ---------------------------------------------------------- Formulario cambiar perfil ----------------------------------------------------------
		}elseif(isset($_POST["cambiar_datos"])){
			$id_usuario = $_POST["id_usuario"];
			
			$sql = "SELECT avatar FROM usuarios WHERE id_usuario=".$id_usuario;
			$resultados = $db->query($sql);
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$avatar = "{$avatar}";
			}
			
			$nombre = $_FILES['avatar']['name'];
			$tmp_name = $_FILES['avatar']['tmp_name'];
			
			$error = $_FILES['avatar']['error'];
			$size = $_FILES['avatar']['size'];
			$max_size = 1024 * 1024 * 1;
			$type = $_FILES['avatar']['type'];
			
			if(!$error){
				if ($error){
					$titulo = "Error";
					$subtitulo = "Se ha producido un error";
					$tipo = "error";
					require "toastr.php";
				}else if ($size > $max_size){
					$titulo = "Error";
					$subtitulo = "Error, el tamaño máximo permitido es un 1MB";
					$tipo = "error";
					require "toastr.php";
				}else if($type != "image/jpeg" && $type != "image/jpg" && $type != "image/png" && $type != "image/gif"){
					$titulo = "Error";
					$subtitulo = "El archivo no es una imagen";
					$tipo = "error";
					require "toastr.php";
				}else{
					$ruta = "galerias/fotos_".$_SESSION['usr']."/avatar/".$nombre;
					if($avatar != ""){
						unlink("galerias/fotos_".$_SESSION['usr']."/avatar/".$avatar);
					}
					move_uploaded_file($tmp_name, $ruta);
					
					$editar = "UPDATE usuarios SET avatar=:avatar WHERE id_usuario=:id_usuario";
					$result = $db->prepare($editar);
					$result->execute(array(":avatar" => $nombre, ":id_usuario" => $id_usuario));
				}
			}else{
				$titulo = "Error";
				$subtitulo = "Se ha producido un error";
				$tipo = "error";
			}
			
			
			if($_POST["nombre"] != ""){
				$editar = "UPDATE usuarios SET nombre=:nombre WHERE id_usuario=:id";
				$result = $db->prepare($editar);
				$result->execute(array(":nombre" => $_POST["nombre"], ":id" => $id_usuario));
			}
			if($_POST["email"] != ""){
				try{
					$editar = "UPDATE usuarios SET email=:email WHERE id_usuario=:id";
					$result = $db->prepare($editar);
					$result->execute(array(":email" => $_POST["email"], ":id" => $id_usuario));
				}catch(Exception $e){
					$titulo = "Error";
					$subtitulo = "Este e-mail ya existe";
					$tipo = "info";
					require "toastr.php";
				}
			}

			$editar = "UPDATE usuarios SET nacionalidad=:nacionalidad WHERE id_usuario=:id";
			$result = $db->prepare($editar);
			$result->execute(array(":nacionalidad" => $_POST["nacionalidad"], ":id" => $id_usuario));
			
			$editar = "UPDATE usuarios SET genero=:genero WHERE id_usuario=:id";
			$result = $db->prepare($editar);
			$result->execute(array(":genero" => $_POST["genero"], ":id" => $id_usuario));
			
			if($_POST["telefono"] != ""){
				try{
					$editar = "UPDATE usuarios SET telefono=:telefono WHERE id_usuario=:id";
					$result = $db->prepare($editar);
					$result->execute(array(":telefono" => $_POST["telefono"], ":id" => $id_usuario));
				}catch(Exception $e){
					$titulo = "Error";
					$subtitulo = "Eso no es un número de teléfono";
					$tipo = "info";
					require "toastr.php";
				}
			}
			if($_POST["fecha_nacimiento"] != ""){
				$editar = "UPDATE usuarios SET fecha_nacimiento=:fecha_nacimiento WHERE id_usuario=:id";
				$result = $db->prepare($editar);
				$result->execute(array(":fecha_nacimiento" => $_POST["fecha_nacimiento"], ":id" => $id_usuario));
			}
			if($_POST["vieja_contrasena"] != ""){
				
				$sql = "SELECT * FROM usuarios WHERE usuario='".$_SESSION['usr']."'";
				$resultados = $db->query($sql);
				while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
					extract($row);
					
					if(crypt($_POST['vieja_contrasena'], Encrypter::decrypt("{$clave}")) == Encrypter::decrypt("{$clave}")){
						if($_POST["repetir_contrasena"] != ""){
							$editar = "UPDATE usuarios SET clave=:clave WHERE usuario=:usuario";
							$result = $db->prepare($editar);
							$result->execute(array(":clave" => Encrypter::encrypt(cryptPass($_POST["repetir_contrasena"])), ":usuario" => $_SESSION['usr']));
							
							$titulo = "Correcto";
							$subtitulo = "Contraseña cambiada";
							$tipo = "success";
							require "toastr.php";
						}
					}else{
						$titulo = "Error";
						$subtitulo = "Contraseña incorrecta";
						$tipo = "error";
						require "toastr.php";
					}
				}
			}
			
			if($_POST["repetir_contrasena"] != ""){
				if($_POST["vieja_contrasena"] == ""){
					$titulo = "Error";
					$subtitulo = "Mete la antigua contraseña";
					$tipo = "warning";
					require "toastr.php";
				}
			}
			
			if($_POST["descripcion"] != ""){
				$editar = "UPDATE usuarios SET descripcion=:descripcion WHERE id_usuario=:id";
				$result = $db->prepare($editar);
				$result->execute(array(":descripcion" => $_POST["descripcion"], ":id" => $id_usuario));
			}
			
			$sql = "SELECT * FROM usuarios WHERE usuario='".$_SESSION['usr']."'";
			$consulta   = $db->query($sql);
			$resultados = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$datos_usuario[] = $row;
			}
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$id_usuario = "{$id_usuario}";
			}
			
			$sql = "SELECT * FROM temas";
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$temas[] = $row;
			}
			
			$sql = "SELECT * FROM amigos_usuario WHERE id_usuario=".$id_usuario;
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$amigos_usuario[] = $row;
			}
		// ---------------------------------------------------------- Fin formulario cambiar perfil ----------------------------------------------------------
		// ---------------------------------------------------------- Chat ----------------------------------------------------------
		}elseif(isset($_POST["cerrar_chat"])){
			$sql = "SELECT * FROM usuarios WHERE usuario='".$_SESSION['usr']."'";
			$consulta   = $db->query($sql);
			$resultados = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$datos_usuario[] = $row;
			}
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$id_usuario = "{$id_usuario}";
			}
			
			$sql = "SELECT * FROM temas";
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$temas[] = $row;
			}
			
			$sql = "SELECT * FROM amigos_usuario WHERE id_usuario=".$id_usuario;
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$amigos_usuario[] = $row;
			}
			
			unset($_SESSION["chat"]); 
			session_destroy();
			
			header("Location: https://localhost/");
			exit;
		
		}elseif(isset($_SESSION['chat'])){
			$sql = "SELECT * FROM usuarios WHERE usuario='".$_SESSION['usr']."'";
			$consulta   = $db->query($sql);
			$resultados = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$datos_usuario[] = $row;
			}
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$id_usuario = "{$id_usuario}";
			}
			
			$sql = "SELECT * FROM temas";
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$temas[] = $row;
			}
			
			$sql = "SELECT * FROM amigos_usuario WHERE id_usuario=".$id_usuario;
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$amigos_usuario[] = $row;
			}
			
			if ($_SESSION['chat'] != "chat"){
				if($_SESSION['chat'] != ""){
					$numero = $_SESSION['chat'];
				}else{
					$titulo = "Error";
					$subtitulo = "Mete un codigo para ir a la sala";
					$tipo = "warning";
					require "toastr.php";
				}
			}
			
		// ---------------------------------------------------------- Fin chat ----------------------------------------------------------
		// ---------------------------------------------------------- Principal ----------------------------------------------------------
		}else{
			$sql = "SELECT * FROM usuarios WHERE usuario='".$_SESSION['usr']."'";
			$consulta   = $db->query($sql);
			$resultados = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$datos_usuario[] = $row;
			}
			while ($row = $resultados->fetch(PDO::FETCH_ASSOC)) {
				extract($row);
				$id_usuario = "{$id_usuario}";
			}
			
			$sql = "SELECT * FROM temas";
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$temas[] = $row;
			}
			
			$sql = "SELECT * FROM amigos_usuario WHERE id_usuario=".$id_usuario;
			$consulta = $db->query($sql);
			while ($row = $consulta->fetchObject()) {
				$amigos_usuario[] = $row;
			}
		}
		// ---------------------------------------------------------- Fin principal ----------------------------------------------------------
		if(!isset($amigos_usuario)){
			$amigos_usuario = "vacio";
		}
		
		
		
		
		
		
		// ---------------------------------------------------------- Templates ----------------------------------------------------------
		// ---------------------------------------------------------- Templates ----------------------------------------------------------
		// ---------------------------------------------------------- Templates ----------------------------------------------------------
		
		
	}
	
	unset($db); 

	$loader = new Twig_Loader_Filesystem('templates/');
  
	$twig = new Twig_Environment($loader);
	
	// ---------------------------------------------------------- No registrado ----------------------------------------------------------
	if(!isset($_SESSION['usr'])){
	
		if(isset($_POST['registrarse'])){
			$publickey = "6Ld_s_0SAAAAAO4tWX8h9dtaPRr8OHVN5vsv-Pw5";
			
			$template = $twig->loadTemplate('alta.html');

			echo $template->render(array(
				'titulo' => 'Alta',
				'recaptcha' => recaptcha_get_html($publickey,'',true)
			));
		}elseif(isset($texto_informar)){
			$template = $twig->loadTemplate('confirmar.html');

			echo $template->render(array(
				'titulo' => 'Confirmar por e-mail'
			));
		}else{
			$template = $twig->loadTemplate('registrarse.html');

			echo $template->render(array(
				'titulo' => 'Registrarse'
			));
		}
	}else{  // ---------------------------------------------------------- No registrado ----------------------------------------------------------
		
		if((isset($_POST["nuevo_tema"])) && (!isset($nuevo_tema))){
			$template = $twig->loadTemplate('foro.html');

			if(isset($comentarios)){
				echo $template->render(array(
					'titulo' => $_POST["titulo"].' - Foro',
					'comentarios' => $comentarios,
					'datos_usuario' => $datos_usuario,
					'usuario' => $_SESSION['usr'],
					'id_tema' => $id_tema,
					'temas' => $temas,
					'amigos_usuario' => $amigos_usuario
				));
			}else{
				echo $template->render(array(
					'titulo' => $_POST["titulo"].' - Foro',
					'datos_usuario' => $datos_usuario,
					'usuario' => $_SESSION['usr'],
					'id_tema' => $id_tema,
					'temas' => $temas,
					'amigos_usuario' => $amigos_usuario
				));
			}
		}elseif((isset($_POST["ver_tema"])) || (isset($_POST["comentar"])) || (isset($_POST["recargar_foro"]))){
			$template = $twig->loadTemplate('foro.html');
			
			if(isset($comentarios)){
				echo $template->render(array(
					'titulo' => 'Foro',
					'comentarios' => $comentarios,
					'datos_usuario' => $datos_usuario,
					'usuario' => $_SESSION['usr'],
					'id_tema' => $id_tema,
					'temas' => $temas,
					'amigos_usuario' => $amigos_usuario
				));
			}else{
				echo $template->render(array(
					'titulo' => 'Foro',
					'datos_usuario' => $datos_usuario,
					'usuario' => $_SESSION['usr'],
					'id_tema' => $id_tema,
					'temas' => $temas,
					'amigos_usuario' => $amigos_usuario
				));
			}
		}elseif((isset($_POST["galeria"])) || (isset($_POST["recargar_galeria"])) || (!empty($_POST["tipo_galeria"]))){
			$template = $twig->loadTemplate('galeria.html');
			if (isset($_POST["seguirBorrando"])){
				if(isset($portada)){
					echo $template->render(array(
						'titulo' => 'Galeria de '.$usuario,
						'galeria' => $galeria,
						'datos_usuario' => $datos_usuario,
						'usuario' => $_SESSION['usr'],
						'usuarioGaleria' => $usuario,
						'portada' => $portada,
						'id_usuario' => $id_usuario,
						'temas' => $temas,
						'amigos_usuario' => $amigos_usuario,
						'tipo_galeria' => $tipo_galeria,
						'borrar' => 'si'
					));
				}else{
					echo $template->render(array(
						'titulo' => 'Galeria de '.$usuario,
						'galeria' => $galeria,
						'datos_usuario' => $datos_usuario,
						'usuario' => $_SESSION['usr'],
						'usuarioGaleria' => $usuario,
						'id_usuario' => $id_usuario,
						'temas' => $temas,
						'amigos_usuario' => $amigos_usuario,
						'tipo_galeria' => $tipo_galeria,
						'borrar' => 'si'
					));
				}
			}elseif(isset($_POST["continuar"])){
				if(isset($_POST["galeria"])){
					if(isset($portada)){
						echo $template->render(array(
							'titulo' => 'Galeria de '.$usuario,
							'galeria' => $galeria,
							'datos_usuario' => $datos_usuario,
							'usuario' => $_SESSION['usr'],
							'usuarioGaleria' => $usuario,
							'portada' => $portada,
							'id_usuario' => $id_usuario,
							'temas' => $temas,
							'amigos_usuario' => $amigos_usuario,
							'tipo_galeria' => $tipo_galeria
						));
					}else{
						echo $template->render(array(
							'titulo' => 'Galeria de '.$usuario,
							'galeria' => $galeria,
							'datos_usuario' => $datos_usuario,
							'usuario' => $_SESSION['usr'],
							'usuarioGaleria' => $usuario,
							'id_usuario' => $id_usuario,
							'temas' => $temas,
							'amigos_usuario' => $amigos_usuario,
							'tipo_galeria' => $tipo_galeria
						));
					}
				}else{
					if(isset($portada)){
						echo $template->render(array(
							'titulo' => 'Galeria de '.$usuario,
							'galeria' => $galeria,
							'datos_usuario' => $datos_usuario,
							'usuario' => $_SESSION['usr'],
							'usuarioGaleria' => $usuario,
							'portada' => $portada,
							'id_usuario' => $id_usuario,
							'temas' => $temas,
							'amigos_usuario' => $amigos_usuario,
							'tipo_galeria' => $tipo_galeria,
							'borrar' => 'si'
						));
					}else{
						echo $template->render(array(
							'titulo' => 'Galeria de '.$usuario,
							'galeria' => $galeria,
							'datos_usuario' => $datos_usuario,
							'usuario' => $_SESSION['usr'],
							'usuarioGaleria' => $usuario,
							'id_usuario' => $id_usuario,
							'temas' => $temas,
							'amigos_usuario' => $amigos_usuario,
							'tipo_galeria' => $tipo_galeria,
							'borrar' => 'si'
						));
					}
				}
			}else{
				if(isset($amigo)){
					if(isset($portada)){
						echo $template->render(array(
							'titulo' => 'Galeria de '.$usuario,
							'galeria' => $galeria,
							'datos_usuario' => $datos_usuario,
							'usuario' => $_SESSION['usr'],
							'usuarioGaleria' => $usuario,
							'portada' => $portada,
							'id_usuario' => $id_usuario,
							'temas' => $temas,
							'amigos_usuario' => $amigos_usuario,
							'tipo_galeria' => $tipo_galeria,
							'amigo' => $amigo
						));
					}else{
						echo $template->render(array(
							'titulo' => 'Galeria de '.$usuario,
							'galeria' => $galeria,
							'datos_usuario' => $datos_usuario,
							'usuario' => $_SESSION['usr'],
							'usuarioGaleria' => $usuario,
							'id_usuario' => $id_usuario,
							'temas' => $temas,
							'amigos_usuario' => $amigos_usuario,
							'tipo_galeria' => $tipo_galeria,
							'amigo' => $amigo
						));
					}
				}else{
					if(isset($portada)){
						echo $template->render(array(
							'titulo' => 'Galeria de '.$usuario,
							'galeria' => $galeria,
							'datos_usuario' => $datos_usuario,
							'usuario' => $_SESSION['usr'],
							'usuarioGaleria' => $usuario,
							'portada' => $portada,
							'id_usuario' => $id_usuario,
							'temas' => $temas,
							'amigos_usuario' => $amigos_usuario,
							'tipo_galeria' => $tipo_galeria
						));
					}else{
						echo $template->render(array(
							'titulo' => 'Galeria de '.$usuario,
							'galeria' => $galeria,
							'datos_usuario' => $datos_usuario,
							'usuario' => $_SESSION['usr'],
							'usuarioGaleria' => $usuario,
							'id_usuario' => $id_usuario,
							'temas' => $temas,
							'amigos_usuario' => $amigos_usuario,
							'tipo_galeria' => $tipo_galeria
						));
					}
				}
			}
		}elseif(isset($_POST["subirImagenGaleria"])){
			$template = $twig->loadTemplate('galeria.html');

			if(isset($portada)){
				if(isset($_POST["continuar"])){
					echo $template->render(array(
						'titulo' => 'Galeria de '.$usuario,
						'galeria' => $galeria,
						'datos_usuario' => $datos_usuario,
						'usuario' => $_SESSION['usr'],
						'usuarioGaleria' => $usuario,
						'portada' => $portada,
						'id_usuario' => $id_usuario,
						'borrar' => 'si',
						'temas' => $temas,
						'amigos_usuario' => $amigos_usuario,
						'tipo_galeria' => $tipo_galeria
					));
				}else{
					echo $template->render(array(
						'titulo' => 'Galeria de '.$usuario,
						'galeria' => $galeria,
						'datos_usuario' => $datos_usuario,
						'usuario' => $_SESSION['usr'],
						'usuarioGaleria' => $usuario,
						'portada' => $portada,
						'id_usuario' => $id_usuario,
						'tipo_galeria' => $tipo_galeria
					));
				}
			}else{
				if(isset($_POST["continuar"])){
					echo $template->render(array(
						'titulo' => 'Galeria de '.$usuario,
						'galeria' => $galeria,
						'datos_usuario' => $datos_usuario,
						'usuario' => $_SESSION['usr'],
						'usuarioGaleria' => $usuario,
						'id_usuario' => $id_usuario,
						'borrar' => 'si',
						'temas' => $temas,
						'amigos_usuario' => $amigos_usuario,
						'tipo_galeria' => $tipo_galeria
					));
				}else{
					echo $template->render(array(
						'titulo' => 'Galeria de '.$usuario,
						'galeria' => $galeria,
						'datos_usuario' => $datos_usuario,
						'usuario' => $_SESSION['usr'],
						'usuarioGaleria' => $usuario,
						'id_usuario' => $id_usuario,
						'temas' => $temas,
						'amigos_usuario' => $amigos_usuario,
						'tipo_galeria' => $tipo_galeria
					));
				}
			}
		}elseif((isset($_POST["modoBorrar"])) || (isset($_POST["borrar_imagen"]))){
			$template = $twig->loadTemplate('galeria.html');

			if(isset($portada)){
				echo $template->render(array(
					'titulo' => 'Galeria de '.$usuario,
					'galeria' => $galeria,
					'datos_usuario' => $datos_usuario,
					'usuario' => $_SESSION['usr'],
					'usuarioGaleria' => $usuario,
					'portada' => $portada,
					'id_usuario' => $id_usuario,
					'borrar' => 'si',
					'temas' => $temas,
					'amigos_usuario' => $amigos_usuario,
					'tipo_galeria' => $tipo_galeria
				));
			}else{
				echo $template->render(array(
					'titulo' => 'Galeria de '.$usuario,
					'galeria' => $galeria,
					'datos_usuario' => $datos_usuario,
					'usuario' => $_SESSION['usr'],
					'usuarioGaleria' => $usuario,
					'id_usuario' => $id_usuario,
					'borrar' => 'si',
					'temas' => $temas,
					'amigos_usuario' => $amigos_usuario,
					'tipo_galeria' => $tipo_galeria,
					'amigo' => $tipo_galeria
				));
			}
		}elseif((isset($_POST["datos_personales"])) || (isset($_POST["cambiar_datos"])) || (isset($_POST["recargar_formulario"]))){
			$template = $twig->loadTemplate('formulario.html');

			echo $template->render(array(
				'titulo' => 'Modificar perfil',
				'usuario' => $_SESSION['usr'],
				'datos_usuario' => $datos_usuario,
				'temas' => $temas,
				'amigos_usuario' => $amigos_usuario
			));
		}elseif(isset($_SESSION['chat'])){
			$template = $twig->loadTemplate('chat.html');

			if(isset($numero)){
				echo $template->render(array(
					'titulo' => 'Chat',
					'datos_usuario' => $datos_usuario,
					'temas' => $temas,
					'amigos_usuario' => $amigos_usuario,
					'numero' => $numero
				));
			}else{
				echo $template->render(array(
					'titulo' => 'Chat',
					'datos_usuario' => $datos_usuario,
					'temas' => $temas,
					'amigos_usuario' => $amigos_usuario
				));
			}
		}elseif(isset($_POST["cerrar"])){
			$template = $twig->loadTemplate('registrarse.html');

			echo $template->render(array(
				'titulo' => 'Registrarse'
			));
		}else{
			$template = $twig->loadTemplate('principal.html');
			
			if(isset($amigos_usuario)){
				echo $template->render(array(
					'titulo' => 'Bienvenido',
					'datos_usuario' => $datos_usuario,
					'temas' => $temas,
					'amigos_usuario' => $amigos_usuario
				));
			}else{
				echo $template->render(array(
					'titulo' => 'Bienvenido',
					'datos_usuario' => $datos_usuario,
					'temas' => $temas
				));
			}
		}
	}

} catch (Exception $e) {
	die ('ERROR: ' . $e->getMessage());
}

?>