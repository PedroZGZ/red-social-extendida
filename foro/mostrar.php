<?php
echo $_POST['ckeditor'];

echo "<br>";
echo "---------------------------------------------";
echo "<br>";

date_default_timezone_set('Europe/Madrid');

$Hora = Time(); // Hora actual   
echo date('H:i:s',$Hora);      

$Hora = Time() + (60 *60 * -1);   
echo "<br>";
echo date('H:i:s',$Hora); //  -1 hora

$Hora = Time() + (60 *60 * 2);   
echo "<br>";
echo date('H:i:s',$Hora); //  +2 hora

echo "<br>";
echo "---------------------------------------------";
echo "<br>";


//-----------------
function RestarFechas($fecha1, $fecha2, $absoluto = true){
	if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha1))

	list($año1,$mes1,$dia1)= explode("/",$fecha1);

	if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha1))
		//list($año1,$mes1,$dia1)=split("-",$fecha1);
		$Res1	=	explode("-",$fecha1);	//	0 = Año ; 1 = Mes ; 2 = Dia

	if (preg_match("/[0-9]{1,2}\/[0-9]{1,2}\/([0-9][0-9]){1,2}/",$fecha2))
		//list($año2,$mes2,$dia2)=explode("/",$fecha2);
		$Res2	=	explode("-",$fecha2);	//	0 = Año ; 1 = Mes ; 2 = Dia

	if (preg_match("/[0-9]{1,2}-[0-9]{1,2}-([0-9][0-9]){1,2}/",$fecha2))
		//list($año2,$mes2,$dia2)=explode("-",$fecha2);
		$Res3	=	explode("-",$fecha2);	//	0 = Año ; 1 = Mes ; 2 = Dia

	$dif = mktime(0,0,0,$Res1[1],$Res1[2],$Res1[0]) - mktime(0,0,0,$Res3[1],$Res3[2],$Res3[0]);


	$dif = $dif / (60*60*24);

	$dif = ($absoluto)?abs($dif):$dif; 
	$ndias = floor($dif);

	return($ndias);
}


// Asi es como se implementa
echo "Resta de fechas:";
echo "<br>";
echo RestarFechas(date('Y-m-d'),'2014-11-10', $absoluto = true);

echo "<br>";
echo "---------------------------------------------";
echo "<br>";
function RestarHoras($HorayMin , $Add_hora = false , $Add_mint = false ){
	// SUMAR HORAS Y MINUTOS A LA HORA

	// $Hora	=	"08:30"; 
	// supongamos que esta es la hora de inicio a la cual sumaremos el tiempo deseado
	// bien ahora vamos a partir la cadena para poder sumarle el tiempo ya sea para los
	// hora o para los minutos

	$Hrs 	= 	explode(':', $HorayMin);

	//usamos la funcion mktime para convertir nuestro tiempo a fecha y poder darle un formato deseado

	// Con la siguiente sentencia sumaria
	//$hora2	=	date("H:i", mktime($Hrs[0]+$Add_hora, $Hrs[1]+$Add_mint, 0));

	// Con la sisguiente sentencia restaria
	$hora2	=	date("H:i", mktime($Hrs[0]+$Add_hora, $Hrs[1]-$Add_mint, 0));
	//Vamos a imprimir la variable para ver que nos arroja
	return $hora2;
	//bien espero no fallar en este script y que les sirva de algo
}

// Salida: Le sumo 10 minutos a la hora actual
// con cambiar los calores en los parametros consigues sumar o restar horas o minutos
echo "Restar horas:";
echo "<br>";
echo RestarHoras(date('H:i') , 1 , 20);

echo "<br>";
echo "---------------------------------------------";
echo "<br>";
echo "Hora actual:";
echo "<br>";

$hora_actual = date('H:i'); //Hora actual
echo $hora_actual;

echo "<br>";
echo "---------------------------------------------";
echo "<br>";
echo "Fecha de hoy:";
echo "<br>";

$fecha_actual = date('Y-m-d'); //Fecha de hoy
echo $fecha_actual;
?>