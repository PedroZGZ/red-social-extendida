/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'es';
	// config.uiColor = '#AADC6E';
	//config.enterMode = CKEDITOR.ENTER_BR;
	//config.height        = '80px';
	//config.width        = '490px';
	//config.toolbar_Full
	config.skin = 'moonocolor';
	config.extraPlugins = 'ckeditor-gwf-plugin,quicktable,wenzgmap,lineheight,youtube,spoiler,xmas,qrc,texttransform,slideshow,lightbox,codeTag';
	


	//ckeditor-gwf-plugin
	config.colorButton_colors = '4A5A73,c54747,00773d,555555';
	config.font_names = 'GoogleWebFonts;' + config.font_names;

	//KCfinder
	config.filebrowserBrowseUrl = '/ckeditor/kcfinder/browse.php';
	config.filebrowserImageBrowseUrl = '/ckeditor/kcfinder/browse.php?type=images';
	config.filebrowserFlashBrowseUrl = '/ckeditor/kcfinder/browse.php?type=flash';
	config.filebrowserUploadUrl = '/ckeditor/kcfinder/upload.php?type=files';
	config.filebrowserImageUploadUrl = '/ckeditor/kcfinder/upload.php?type=images';
	config.filebrowserFlashUploadUrl = '/ckeditor/kcfinder/upload.php?type=flash';

	//youtube
	config.youtube_width = '540'; //Video width
	config.youtube_height = '380'; //Video height
	config.youtube_related = true; //Show related videos
	config.youtube_older = false; //Use old embed code
	config.youtube_privacy = false; //Enable privacy-enhanced mode
	config.youtube_autoplay = false; //Start video automatically

	config.allowedContent = true;

	//xmas
	xmas_wishes: '<p class="big">Ho! Ho! Ho!</p>';
	xmas_signature: 'Santa Claus';
	xmas_link: 'en.wikipedia.org/wiki/Santa_Claus';
};
