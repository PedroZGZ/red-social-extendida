$(function () {
    $("#subir").change(function () {
        $("#previsualizar").html("");
        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.gif|.png|.bmp)$/;
        if (regex.test($(this).val().toLowerCase())) {
            if ($.browser.msie && parseFloat(jQuery.browser.version) <= 9.0) {
                $("#previsualizar").show();
                $("#previsualizar")[0].filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = $(this).val();
			}
            else {
                if (typeof (FileReader) != "undefined") {
                    $("#previsualizar").show();
                    $("#previsualizar").append("<img class='vista_previa' />");
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#previsualizar img").attr("src", e.target.result);
						$(".vista_previa").css("background", "none");
                    }
                    reader.readAsDataURL($(this)[0].files[0]);
                } else {
                    alert("Este buscador no soporta la imagen.");
                }
            }
        } else {
            alert("Por favor sube una imagen valida.");
        }
    });
});