<?php
	function cryptPass($input, $rounds = 9){
		$salt= "";
		$saltChars = array_merge(range("A","Z"), range("a","z"), range(0,9));
		for($i = 0; $i < 22; $i++){
			$salt .= $saltChars[array_rand($saltChars)];
		}
		return crypt($input, sprintf("$2y$%02d$", $rounds) . $salt);
	}

	class Encrypter {
		private static $Key = "secreto";

		public static function encrypt ($input) {
			$output = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(Encrypter::$Key), $input, MCRYPT_MODE_CBC, md5(md5(Encrypter::$Key))));
			return $output;
		}

		public static function decrypt ($input) {
			$output = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(Encrypter::$Key), base64_decode($input), MCRYPT_MODE_CBC, md5(md5(Encrypter::$Key))), "\0");
			return $output;
		}
	}
?>